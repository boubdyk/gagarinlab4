package cacka.labs.lab7;

public class Employee {

	private String employeeName;
	private String employeePostID;
	private String employeeDepartment;
	private String employeeDateOfBirth;

	public String getEmployeeName() {
		return employeeName;
	}

	public String getEmployeePostID() {
		return employeePostID;
	}

	public String getEmployeeDepartment() {
		return employeeDepartment;
	}

	public String getEmployeeDateOfBirth() {
		return employeeDateOfBirth;
	}

	public Employee(String employeeName, String employeePostID,
			String employeeDepartment, String employeeDateOfBirth) {
		super();
		this.employeeName = employeeName;
		this.employeePostID = employeePostID;
		this.employeeDepartment = employeeDepartment;
		this.employeeDateOfBirth = employeeDateOfBirth;
	}

}
