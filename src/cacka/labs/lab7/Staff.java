package cacka.labs.lab7;

public interface Staff {	
	String[] getEmployeeByName(String name);
	String[] getEmployeesByPost(int postID);
}
