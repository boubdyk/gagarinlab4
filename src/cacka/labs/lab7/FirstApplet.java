package cacka.labs.lab7;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class FirstApplet extends Applet implements Staff, ActionListener {

	TextField tf;
	Button btn;
	TextArea ta;
	Label lb;
	Employee a = new Employee("Pechkin", "Buhgalteria", "GlavBuh", "10/10/1980");
	Employee b = new Employee("Pupkin", "Inzhenernyi otdel",
			"Glavnyi Inzhener", "21/11/1977");
	Employee c = new Employee("Sharikov", "Rukovodstvo", "Direktor",
			"11/05/1983");
	Employee d = new Employee("Bublikov", "Servis", "Raznoschik kofe",
			"03/09/1979");
	ArrayList<Employee> employes = new ArrayList<Employee>();
	{
		employes.add(a);
		employes.add(b);
		employes.add(c);
		employes.add(d);
	}

	@Override
	public void init() {
		setLayout(null);
		setSize(900, 900);
		lb = new Label("Enter surname of employee:");
		tf = new TextField(30);
		btn = new Button("Search employee by surname");
		ta = new TextArea(" ", 5, 40);
		lb.setBounds(20, 50, 170, 30);
		tf.setBounds(20, 80, 180, 30);
		btn.setBounds(220, 80, 250, 30);
		ta.setBounds(20, 120, 700, 100);
		add(lb);
		add(tf);
		add(btn);
		add(ta);
	}

	@Override
	public void start() {
		btn.addActionListener(this);
	}

	public String[] getEmployeeByName(String employeeName) {
		String[] str = new String[4];
		for (int i = 0; i < employes.size(); i++) {
			if (employeeName.equals(employes.get(i).getEmployeeName())) {
				str[0] = employes.get(i).getEmployeeName();
				str[1] = employes.get(i).getEmployeeDepartment();
				str[2] = employes.get(i).getEmployeePostID();
				str[3] = employes.get(i).getEmployeeDateOfBirth();
			}
		}
		return str;
	}

	public String[] getEmployeesByPost(int postID) {
		throw new UnsupportedOperationException("Not supported yet!!!");
	}

	public void actionPerformed(ActionEvent e) {
		ta.setText("");
		String[] tmp = getEmployeeByName(tf.getText());
		if (tmp[0] != null) {
			JOptionPane.showMessageDialog(null, "Employee was found :D");
			ta.append("|_______Name_______|______Department____|_________Post_________|______DateOfBirth______|\n");
			for (int i = 0; i < tmp.length; i++) {
				ta.append("|              " + tmp[i] + "               ");
			}
			ta.append("|");
			ta.append("\n------------------------------------------------------------------------------------------------------------------------------------------------");
		} else {
			JOptionPane.showMessageDialog(null, "Employee wasn't found!!!");
		}
	}
}
