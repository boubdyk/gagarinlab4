package cacka.labs.lab6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StringOperations {
	private void doOperations(String str) {
		str = str.replaceAll(" ", "");
		char[] ch = str.toCharArray();
		StringBuffer bf = new StringBuffer();
		ArrayList<Character> endCharArray = new ArrayList<Character>();
		for (int i = 0; i < ch.length; i++) {
			endCharArray.add(ch[i]);
			for (int j = i; j < ch.length; j++) {
				if (endCharArray.get(i).equals(ch[j])) {
					ch[j] = ' ';
				}
			}
		}

		for (int i = 0; i < endCharArray.size(); i++) {
			if (endCharArray.get(i) != ' ') {
				bf.append(endCharArray.get(i));
			}
		}
		System.out.println(bf);
		countElements(str.toCharArray(), bf.toString().toCharArray());
	}

	// Count amount of each element
	private void countElements(char[] masFirst, char[] masLast) {
		int amount;
		for (int i = 0; i < masLast.length; i++) {
			amount = 0;
			for (int j = 0; j < masFirst.length; j++) {
				if (masLast[i] == masFirst[j]) {
					amount++;
				}
			}
			System.out.println(masLast[i] + " = " + amount);
		}
	}

	public static void main(String[] args) {
		StringOperations st = new StringOperations();
		BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
		try {
			st.doOperations(bf.readLine().toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
