package cacka.labs.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class DrawFigure extends JPanel {

	private void doDrawint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		BasicStroke bs3 = new BasicStroke(8, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND);
		g2d.setStroke(bs3);
		switch (MoveImage.cvet) {
		case "Black":
			g2d.setColor(Color.BLACK);
			break;
		case "Red":
			g2d.setColor(Color.RED);
			break;
		case "Blue":
			g2d.setColor(Color.BLUE);
			break;
		case "Green":
			g2d.setColor(Color.GREEN);
			break;
		}
		g2d.fillRect(MoveImage.moveX, MoveImage.moveY, 100, 100);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		doDrawint(g);
		super.repaint();
	}

}
