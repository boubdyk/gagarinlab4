package cacka.labs.lab8;

import java.awt.Color;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MoveImage extends JFrame {
	private JPanel mainPanel;
	// private DrawFigure mainPanel1;
	private JButton btnUp;
	private JButton btnDown;
	private JButton btnLeft;
	private JButton btnRight;
	private JLabel lbColor;
	String[] colors = { "Black", "Red", "Green", "Blue" };
	JComboBox boxColors = new JComboBox(colors);;

	static int moveX = 100;
	static int moveY = 150;
	static Color c = Color.BLUE;
	static String cvet = "Black";

	public MoveImage() {
		super("MoveFigure");
		setSize(1200, 800);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		ActionListener boxListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox com = (JComboBox) e.getSource();
				// boxColors = (JComboBox)e.getSource();
				String item = (String) com.getSelectedItem();
				if (item.equals(colors[0])) {
					MoveImage.cvet = "Black";
				}
				if (item.equals(colors[1])) {
					MoveImage.cvet = "Red";
				}
				if (item.equals(colors[2])) {
					MoveImage.cvet = "Green";
				}
				if (item.equals(colors[3])) {
					MoveImage.cvet = "Blue";
				}
			}
		};

		mainPanel = new DrawFigure();
		mainPanel.setVisible(true);
		mainPanel.setLayout(null);
		mainPanel.setSize(1200, 800);

		btnUp = new JButton("UP");
		btnUp.setVisible(true);
		btnUp.setBounds(600, 600, 80, 80);
		btnUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (MoveImage.moveY > 50) {
					MoveImage.moveY -= 40;
				}
			}
		});

		btnDown = new JButton("DOWN");
		btnDown.setVisible(true);
		btnDown.setBounds(600, 680, 80, 80);
		btnDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (MoveImage.moveY < 420) {
					MoveImage.moveY += 40;
				}			
			}
		});

		btnLeft = new JButton("LEFT");
		btnLeft.setVisible(true);
		btnLeft.setBounds(520, 640, 80, 80);
		btnLeft.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (MoveImage.moveX > 30) {
					MoveImage.moveX -= 40;
				}				
			}
		});

		btnRight = new JButton("RIGHT");
		btnRight.setVisible(true);
		btnRight.setBounds(680, 640, 80, 80);
		btnRight.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (MoveImage.moveX < 1010) {
					MoveImage.moveX += 40;
				}
			}
		});

		boxColors.setVisible(true);
		boxColors.setBounds(800, 600, 100, 30);
		boxColors.addActionListener(boxListener);

		mainPanel.add(btnUp);
		mainPanel.add(btnDown);
		mainPanel.add(btnLeft);
		mainPanel.add(btnRight);
		mainPanel.add(boxColors);
		mainPanel.repaint();
		add(mainPanel);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MoveImage mv = new MoveImage();
				mv.setVisible(true);
			}
		});

	}
}
