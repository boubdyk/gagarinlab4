package cacka.labs.lab2;

import java.util.Scanner;

public class Factorial {
	static boolean isOk = false;
	
	public static void main(String[] args) {
		System.out.println("THIS PROGRAM TAKES FACTORIAL FROM DECIMAL INETEGER NUMBER.\n");
		while(!isOk){
			takeFact();
		}
		System.out.println("\nFINITA LA COMEDIA!");
	}	
	
	private static void takeFact(){
		Scanner scn = new Scanner(System.in);
		String str = scn.nextLine();
		if(!isNumber(str)){
			System.out.println("Error: It's not integer");			
			return;
		}		
		int arg = Integer.parseInt(str);
		if (!isPositive(arg)){
			System.out.println("Error: Cannot take factorial from negative number.");
			return;
		}	
		System.out.println("You entered an integer: " + arg);		
		System.out.println(arg + "! = " + fact(arg));		
		isOk = true;		
	}	
	
	private static boolean isPositive(int arg) {
		return arg < 0 ? false : true;
	}
	
	private static boolean isNumber(String arg) {
		try {			
			Integer.parseInt(arg);
			return true;
		} catch(Exception ex){
			return false;
		}		
	}
	
	private static long fact(int arg) {
		long fact = 1;	
		if (arg > 0){
			for (int i = 1; i <= arg; i++) {
				fact *= i;
			}			
		} 
		return fact;		
	}
}
