package cacka.labs.lab9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CreateCatalog extends JFrame {
	private JLabel lbName;
	private JFileChooser jfc;
	private JTextField tf;
	private JButton btnAdd;
	private JPanel mainPanel;

	public CreateCatalog() {
		super("�������� ��������");

		setSize(800, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		mainPanel = new JPanel();
		mainPanel.setSize(800, 600);
		mainPanel.setVisible(true);
		mainPanel.setLayout(null);

		jfc = new JFileChooser();
		jfc.setBounds(10, 20, 500, 400);
		jfc.setVisible(true);

		tf = new JTextField();
		tf.setBounds(170, 450, 200, 20);
		tf.setVisible(true);

		btnAdd = new JButton("������� �������");
		btnAdd.setVisible(true);
		btnAdd.setBounds(380, 450, 150, 20);
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				File file = jfc.getCurrentDirectory();
				String fullPath = "";
				try {
					fullPath = file.getCanonicalPath();
					//System.out.println(file.getPath());
				} catch (IOException ex) {
					ex.printStackTrace();
				}				
				final String path = fullPath;				
				File f = new File(path + "\\" + tf.getText());
				f.mkdirs();
				jfc.updateUI();
			}
		});
		
		lbName = new JLabel("������� ��� ��������:");
		lbName.setVisible(true);
		lbName.setBounds(10, 450, 150, 20);

		mainPanel.add(jfc);
		mainPanel.add(tf);
		mainPanel.add(lbName);
		mainPanel.add(btnAdd);
		add(mainPanel);

	}

	public static void main(String[] args) throws IOException {
		CreateCatalog cc = new CreateCatalog();
		cc.setVisible(true);
	}
}
