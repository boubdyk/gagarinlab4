package cacka.labs.lab5;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class IndexAndValueArray {
	static boolean isOk = false;
	private double amin;
	private double amax;
	private double[] mas;

	public IndexAndValueArray(double amin, double amax, double[] mas) {
		this.amin = amin;
		this.amax = amax;
		this.mas = mas;
	}

	private void doCheckingAndPrint() {
		for (int i = 0; i < this.mas.length; i++) {
			if (this.mas[i] < this.amin || this.mas[i] > this.amax) {
				System.out.println("Index = " + i + "; Value = " + this.mas[i]);
			}
		}
	}

	private static boolean isNumber(String arg) {
		try {
			Integer.parseInt(arg);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
	
	private static boolean isPositive(int arg) {
		return arg < 0 ? false : true;
	}

	public static void main(String[] args) {
		while (!isOk) {
			BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
			try {				
				System.out.println("Enter min border = ");
				String str = bf.readLine();
				if (!isNumber(str)) {
					System.out.println("Error, you should enter an Integer!");
					continue;
				}				
				double amin = Double.parseDouble(str);
			
				System.out.println("Enter max border = ");
				str = bf.readLine();
				if (!isNumber(str)) {
					System.out.println("Error, you should enter an Integer!");
					continue;
				}		
				double amax = Double.parseDouble(str);
				if (amax < amin) {
					System.out.println("Error: max border can't be < min border!");
					continue;
				}
				
				System.out.println("Enter length of array = ");
				str = bf.readLine();
				if (!isNumber(str)) {
					System.out.println("Error, you should enter an Integer!");
					continue;
				}				
				int n = Integer.parseInt(str);
				if (!isPositive(n)) {
					System.out.println("Error, you shoul enter positive length of array!");
					continue;
				}

				System.out.println("Enter array = ");
				double mas[] = new double[n];
				int i = 0;				
				while (i < n){
					str = bf.readLine();
					if (!isNumber(str)) {
						System.out.println("Error, you shoul enter an Integer!");
						continue;
					}
					mas[i] = Double.parseDouble(str);
					i++;
				}
				IndexAndValueArray ind = new IndexAndValueArray(amin, amax, mas);
				ind.doCheckingAndPrint();
				isOk = true;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
