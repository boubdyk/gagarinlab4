package shcherbashyn.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab2 {
	public int temp[] = new int[] { -100, -80, -60, -40, -20, 0, 20, 40, 60,
			80, 100 };
	public float eds[] = new float[] { -5.641f, -4.635f, -3.563f, -2.431f,
			-1.241f, 0, 1.289f, 2.623f, 3.998f, 5.411f, 6.860f };

	public float currentEds;
	float temperature;

	public float getEds(int kod) {
		return -10f + ((float)kod / 25f);
	}

	public int checkEds(float currentEds) {
		if (eds[0] > currentEds || currentEds > eds[eds.length - 1]) {
			System.out.println("Current value is out of diapason!");
			System.exit(0);
		}
		for (int i = 1; i < eds.length; i++) {
			if (eds[i - 1] <= currentEds && currentEds < eds[i]) {
				return i;
			}
		}
		return -1;
	}

	public float getTemp(int kod) {
		currentEds = getEds(kod);
		System.out.println(currentEds);
		int i = checkEds(currentEds);
		if (i == -1) {
			System.out.println("Incorrect diapason!");
			System.exit(0);
		} else {
			temperature = temp[i - 1]
					+ ((temp[i] - temp[i - 1]) * (currentEds - eds[i - 1]))
					/ (eds[i] - eds[i - 1]);
		}
		return temperature;
	}

	public static void main(String[] args) {
		Lab2 lb2 = new Lab2();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		try {
			int kod = Integer.parseInt(reader.readLine());
			System.out.println(lb2.getTemp(kod));
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid format!");
		} catch (IOException e) { /* NOP */
		}
	}
}
