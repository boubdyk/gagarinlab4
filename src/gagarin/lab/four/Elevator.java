package gagarin.lab.four;

public class Elevator {
	public static final int CAPACITY = 5;
	public static final int FIRST_FLOOR = 1;
	public static final int LAST_FLOOR = 5;
	public static boolean up;
	public static int amountPeople;
	public static int currentFloor;
	public static int fromFloor;
	public static int toFloor;
	
	public Elevator(int amountPeople) {
		this.amountPeople = amountPeople;
	}
}
