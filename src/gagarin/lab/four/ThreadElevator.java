package gagarin.lab.four;

public class ThreadElevator extends Thread {
	
	private int floorNumber;
	
	public int getFloorNumber() {
		return floorNumber;
	}

	public ThreadElevator(final int floorNumber) {
		this.floorNumber = floorNumber;
	}

	@Override
	public void run() {
		System.out.println("Current floor: " + getFloorNumber() + "| There are " + MoveElevator.elevator.amountPeople + " people in elevator");
	}

}
