package gagarin.lab.four;

import java.util.Random;

public class Man {
	private int fromFloor;
	private int toFloor;
	
	public Man() {
		setFromFloor();
		setToFloor();
	}
	
	public int getFromFloor() {
		return fromFloor;
	}
	
	public void setFromFloor() {
		int fromFloor;
		Random rnd = new Random();
		fromFloor = rnd.nextInt(5);
		this.fromFloor = fromFloor;
	}
	
	public int getToFloor() {
		return toFloor;
	}
	
	public void setToFloor() {
		int toFloor;
		if (fromFloor != 0) {
			toFloor = 0;
		} else {
			Random rnd = new Random();
			toFloor = rnd.nextInt(5);
			while (toFloor == getFromFloor()) {
				toFloor = rnd.nextInt(5);
			}
		}
		this.toFloor = toFloor;
	}

}
