package gagarin.lab.four;

public class MoveElevator {
	Thread people;
	Thread[] floor = new Thread[5];
	Man man;
	static Elevator elevator;
	static int currentFloor;
	static int menCounter = 0;
	
	
	public MoveElevator() {
		elevator = new Elevator(0);
		people = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					man = new Man();
					menCounter++;
					try {
						move(menCounter);
					} catch (InterruptedException e1) { /*NOP*/ }
				}
			}
		});
		people.start();
	}
	
	void move(int counter) throws InterruptedException {
		synchronized (elevator) {
			
			if (man.getFromFloor() == 0) {
				
				if (currentFloor == 0) {
					elevator.amountPeople+=counter;
					System.out.println("Moving up! ------------------------------------------------");
					for (int i = 0; i <= man.getToFloor(); i++) {
						currentFloor = i;
						floor[i] = new ThreadElevator(currentFloor);
						floor[i].start();
						floor[i].sleep(1000);
					}
					elevator.amountPeople-=counter;
				} 
				
			} 
			if (currentFloor > 0 && currentFloor <= 4) {
				System.out.println("Moving down! -----------------------------------------------");
				for (int i = currentFloor; i >= man.getFromFloor(); i--) {
					currentFloor = i;
					floor[i] = new ThreadElevator(currentFloor);
					floor[i].start();
					floor[i].sleep(1000);
				}
				elevator.amountPeople+=counter;
				for (int i = man.getFromFloor(); i >=0; i--) {
					currentFloor = i;
					floor[i] = new ThreadElevator(currentFloor);
					floor[i].start();
					floor[i].sleep(1000);
				}
				elevator.amountPeople-=counter;
			}
			if (menCounter > 5) { menCounter = 0;}
		}
	}
	
	public static void main(String[] args) {
		MoveElevator mv = new MoveElevator();
	}
}
