package gagarin.lab.four;

public class Moving {
	ThreadElevator[] floor = new ThreadElevator[5];
	Object obj = new Object();
	public static int currentFloor = 0;
	public static int amountPoeple = 0;
	public static int aimFloor = 4;
	public static boolean up = true;
	static Elevator elevator;
	static Man man;
	
	public Moving() {
		elevator = new Elevator(0);
		elevator.currentFloor = 0;
		elevator.amountPeople = 0;
		man = new Man();
		elevator.fromFloor = man.getFromFloor();
		elevator.toFloor = man.getToFloor();
		System.out.println("From " + elevator.fromFloor + " to " + elevator.toFloor);
	}
	
	public static Elevator getElevator() {
		return elevator;
	}
	
	public void moveUp() throws InterruptedException {
		elevator.amountPeople++;
		for (int i = 0; i < elevator.toFloor; i++) {
			elevator.currentFloor = i;
			floor[i] = new ThreadElevator(elevator.currentFloor);
			floor[i].sleep(1000);
			floor[i].start();
		}
		elevator.amountPeople--;
	}
	
	public void moveDown(int fromFloor) throws InterruptedException {
		elevator.amountPeople++;
		for (int i = elevator.fromFloor; i >= 0; i--) {
			elevator.currentFloor = i;
			floor[i] = new ThreadElevator(elevator.currentFloor);
			floor[i].sleep(1000);
			floor[i].start();
		}
		elevator.amountPeople--;
	}
	
	public static void main(String[] args) throws InterruptedException {
		Moving mv = new Moving();
		while (true) {
			if (elevator.currentFloor == 0) {
				mv.moveUp();
			} else {
				mv.moveDown(4);
			}
		}
	}

}
